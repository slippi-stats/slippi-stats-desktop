# Introduction

Slippi Stats is an application to visualize data imported from [Slippi](https://github.com/project-slippi/project-slippi) replay game file (.slp) through various charts and many filtering options.

For example, you can check your winrate with and/or against any character, on any stage, within a specific time span. You can also track your evolution through time.

Slippi Stats is a rebuild of a previous web application named [Slippi Stats Web](https://gitlab.com/Coukaratcha/slippi-stats-web).

# Installation

## Installation from binaries (for users)

*TODO*

## Installation from sources (for developers)

1. Clone the repository

`$ git clone git@gitlab.com:slippi-stats/slippi-stats.git` or `$ git clone https://gitlab.com/slippi-stats/slippi-stats.git`

2. Install dependencies

`$ yarn install`

3. Compile scss into css

`$ yarn scss`

4. Init the database

`$ npx prisma migrate dev`

5. Run the electron app

`$ yarn start`