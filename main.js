const { app, BrowserWindow, ipcMain } = require("electron")
const path = require("path")
const fs = require("fs")
const { PrismaClient } = require("@prisma/client");

const { parse, progress } = require("./scripts/import.js")
const { stats } = require("./app/stats.js")
const { settings } = require("./app/settings.js");

const winston = require("winston")

const logger = winston.createLogger({
	level: "info",
	format: winston.format.json(),
	defaultMeta: { service: "user-service" },
	transports: [
		new winston.transports.File({ filename: "error.log", level: "error" }),
		new winston.transports.File({ filename: "combined.log" })
	]
})

const prisma = new PrismaClient();

function createWindow(){
	const win = new BrowserWindow({
		width: 1440,
		height: 1024,
		webPreferences: {
			preload: "./app/preload.js",
			nodeIntegration: true,
			nodeIntegrationInWorker: true,
			contextIsolation: false
		}
	})

	win.loadFile("./app/index.html")

	win.webContents.openDevTools()
}

app.whenReady().then(() => {
	createWindow()

	app.on("activate", () => {
		if (BrowserWindow.getAllWindows().length === 0){
			createWindow()
		}
	})
})

app.on("window-all-closed", () => {
	if (process.platform !== "darwin"){
		app.quit()
	}
})

ipcMain.on("parse", () => parse(app.isPackaged))
ipcMain.on("progress", (e, args) => {
	e.sender.send("progress", progress)
})

ipcMain.on("settings", settings);

ipcMain.on("stats", stats)