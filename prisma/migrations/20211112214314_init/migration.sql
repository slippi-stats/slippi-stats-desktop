-- CreateTable
CREATE TABLE "Game" (
    "filename" TEXT NOT NULL PRIMARY KEY,
    "status" TEXT NOT NULL DEFAULT 'ok',
    "datetime" DATETIME NOT NULL,
    "opponent" TEXT NOT NULL,
    "opponent_nickname" TEXT NOT NULL,
    "char_player" TEXT NOT NULL,
    "char_opponent" TEXT NOT NULL,
    "score_player" INTEGER NOT NULL,
    "score_opponent" INTEGER NOT NULL,
    "win" BOOLEAN,
    "stage" TEXT NOT NULL,
    "duration" INTEGER NOT NULL
);
