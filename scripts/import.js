const path = require("path");
const fs = require("fs");
const os = require("os");
const { Worker } = require("worker_threads");

const { PrismaClient } = require("@prisma/client");

const prisma = new PrismaClient();

const winston = require("winston")

const logger = winston.createLogger({
	level: "info",
	format: winston.format.json(),
	defaultMeta: { service: "user-service" },
	transports: [
		new winston.transports.File({ filename: "error.log", level: "error" }),
		new winston.transports.File({ filename: "combined.log" })
	]
})

const progress = {
	locked: false,
	total: 0,
	imported: 0,
	ignored:  0,
	errors: 0,
	logs: []
}

const NB_THREADS = os.cpus().length;

const getAllFiles = dir => 
	fs.readdirSync(dir).reduce((files, file) => {
		const name = path.join(dir, file);
		const isDirectory = fs.statSync(name).isDirectory();
		return isDirectory ? [...files, ...getAllFiles(name)] : [...files, {dir: dir, file: file}];
	}, []);

function parse(isPackaged){
	(async () => {
		const settings = await prisma.settings.findMany()
		const conf = {}

		for (s of settings){
			conf[s.key] = s.value
		}

		if (progress.locked){
			logger.info("Process already started")
			return;
		}

		progress.locked = true;
		progress.logs = [];
		progress.imported = 0;
		progress.ignored = 0;
		progress.errors = 0;

		const replays_dir_path = path.join(conf.replays_dir);

		const files = getAllFiles(replays_dir_path).filter(f => f.file.endsWith(".slp"));

		progress.total = files.length;

		const new_files = [];

		for (f of files) {
			const game = await prisma.game.findUnique({
				where: {
					filename: f.file
				}
			})
			
			if (!game) {
				new_files.push(f);
			}
			else{
				progress.logs.push(["INFO", `${f.file} : Already imported`]);
				progress.ignored++;
			}
		}

		const n = new_files.length;

		if (n === 0){
			progress.locked = false;
			return;
		}

		const chunk_size = Math.ceil(n / NB_THREADS);

		const chunks_of_files = [];

		for (var i = 0; i < n; i+=chunk_size){
			chunks_of_files.push(new_files.slice(i, i+chunk_size));
		}

		const workers = [];

		for (c_f of chunks_of_files){
			const script_path = isPackaged
				? path.resolve(process.resourcesPath, "app.asar.unpacked", "parse.js")
				: path.resolve(__dirname, "./parse.js")

			const w = new Worker(script_path, { workerData: { files: c_f, player_code: conf.player_code } })

			w.on("message", async msg => {
				if (!msg.error) {
					await prisma.game.create({
						data: {
							filename: msg.filename,
							status: "ok",
							datetime: msg.datetime,
							opponent: msg.opponent,
							opponent_nickname: msg.opponent_nickname,
							char_player: msg.char_player,
							char_opponent: msg.char_opponent,
							score_player: msg.score_player,
							score_opponent: msg.score_opponent,
							win: msg.win,
							stage: msg.stage,
							duration: msg.duration
						}
					})
					
					progress.imported++;
					progress.logs.push(["SUCCESS", `${msg.filename} : Added successfully`]);
				}
				else{
					progress.logs.push(["ERROR", `${msg.filename} : ${msg.error}`]);
					progress.errors++;
				}

				if (progress.total === (progress.imported + progress.ignored + progress.errors)){
					progress.locked = false;
				}
			});
			w.on("error", err => { logger.error(err); });

			workers.push(w);
		}
	})()
		.catch((e) => {
			throw e; 
		})
		.finally(async () => {
			await prisma.$disconnect();
		});
}

exports.parse = parse;
exports.progress = progress;
