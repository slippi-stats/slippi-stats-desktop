const { workerData, parentPort } = require("worker_threads");
const { SlippiGame } = require("@slippi/slippi-js");
const path = require("path");

const characters = {
	"0": "Mario",
	"1": "Fox",
	"2": "Captain Falcon",
	"3": "Donkey Kong",
	"4": "Kirby",
	"5": "Bowser",
	"6": "Link",
	"7": "Sheik",
	"8": "Ness",
	"9": "Peach",
	"10": "Ice Climbers", // "Popo"
	"11": "Ice Climbers", //"Nana"
	"12": "Pikachu",
	"13": "Samus",
	"14": "Yoshi",
	"15": "Jigglypuff",
	"16": "Mewtwo",
	"17": "Luigi",
	"18": "Marth",
	"19": "Zelda",
	"20": "Young Link",
	"21": "Dr. Mario",
	"22": "Falco",
	"23": "Pichu",
	"24": "Mr. Game & Watch",
	"25": "Ganondorf",
	"26": "Roy"
};

const stages = {
	"2": "Fountain of Dreams",
	"3": "Pokémon Stadium",
	"8": "Yoshi's Story",
	"28": "Dream Land N64",
	"31": "Battlefield",
	"32": "Final Destination"
};

const GameModeVS = 0x02
const GameModeONLINE = 0x08

const endTypes = {
      "TIME" : 1,
      "GAME" : 2,
      "NO_CONTEST" : 7,
};

function get_character_name(id){
	return characters[Object
		.entries(id)
		.sort((a, b) => b[1] - a[1])
		[0]
		[0]
	]
}

const files = workerData.files;
const player_codes = workerData.player_code;
const player_codes_list = player_codes.split(" ");

for (f of files){	
	try{

		const filepath = path.join(f.dir, f.file);

		let game, settings, metadata, stats;

		try{
			game = new SlippiGame(filepath);
			metadata = game.getMetadata();
			settings = game.getSettings();
		}
		catch (e){
			parentPort.postMessage({
				filename: f.file,
				error: "Error while parsing file"
			});
			continue;
		}

		const last_frame = game.getLatestFrame();

		let player_id, opponent_id;

		if (settings.gameMode !== GameModeONLINE){
			parentPort.postMessage({
				filename: f.file,
				error: "Offline game"
			});
			continue;
		}

		if (!metadata){
			parentPort.postMessage({
				filename: f.file,
				error: "Unable to read metadata"
			});
			continue;
		}

		if (Object.keys(metadata.players).length !== 2){
			parentPort.postMessage({
				filename: f.file,
				error: `Not a 1vs1 game (nb player(s): ${Object.keys(metadata.players).length})`
			});
			continue;
		}

		for (id in metadata.players){
			if (player_codes_list.find(player_code => player_code === metadata.players[id].names.code) != undefined){
				player_id = id;
				break;
			}
		}

		if (player_id === undefined){
			parentPort.postMessage({
				filename: f.file,
				error: "Unable to find player's code"
			});
			continue;
		}

		opponent_id = player_id === '0' ? '1' : '0';

		preciseDatetime = new Date(metadata.startAt);
		datetime = new Date(preciseDatetime.getFullYear(), preciseDatetime.getMonth(), preciseDatetime.getDate());

		opponent = metadata.players[opponent_id].names.code;
		opponent_nickname = metadata.players[opponent_id].names.netplay;

		char_player = get_character_name(metadata.players[player_id].characters);
		char_opponent = get_character_name(metadata.players[opponent_id].characters);

		if (!char_player || !char_opponent){
			parentPort.postMessage({
				filename: f.file,
				error: "One of the players used an illegal character"
			});
			continue;
		}

		stage = stages[settings.stageId];

		if (!stage){
			parentPort.postMessage({
				filename: f.file,
				error: "The game was played on an illegal stage"
			});
			continue;
		}
		duration = last_frame.frame;
		
		const end = game.getGameEnd();

		try{
			stats = game.getStats();
		}
		catch (e){
			parentPort.postMessage({
				filename: f.file,
				error: "Error while computing stats"
			});
			continue;
		}

		score_player = stats.stocks.filter(x => x.playerIndex == opponent_id && x.deathAnimation !== null).length
		score_opponent = stats.stocks.filter(x => x.playerIndex == player_id && x.deathAnimation !== null).length

		win = null;

		if (!end){
			parentPort.postMessage({
				filename: f.file,
				error: "The game has no end"
			});
			continue;
		}

		if (end.gameEndMethod === endTypes["GAME"]){
			win = score_player > score_opponent;
		}
		else if (end.gameEndMethod === endTypes["TIME"]){
			if (score_player === score_opponent){
				const percent_player = last_frame.players[player_id]["pre"].percent;
				const percent_opponent = last_frame.players[opponent_id]["pre"].percent;

				if (percent_player < percent_opponent){
					win = true;
				}
				else if (percent_player > percent_opponent){
					win = false;
				}
			}
		}
		else if (end.gameEndMethod === endTypes["NO_CONTEST"]){
			// skip file if no stock is lost and duration is too small
			const limitNbSeconds = 15;
			const limitNbFrames = limitNbSeconds*60;
			if (score_player === 0 &&
				score_opponent === 0 &&
				stats.playableFrameCount < limitNbFrames){
				parentPort.postMessage({
					filename: f.file,
					error: `The game ended with LRAS with no stock lost and last less than ${limitNbSeconds}s`
				});
				continue;
			}
			// otherwise LRAS initiator loses the game
			win = end.lrasInitiatorIndex === opponent_id;
		}

		parentPort.postMessage({
			filename: f.file,
			datetime: datetime,
			opponent: opponent,
			opponent_nickname: opponent_nickname,
			char_player: char_player,
			char_opponent: char_opponent,
			score_player: score_player,
			score_opponent: score_opponent,
			win: win,
			stage: stage,
			duration: duration
		});
	}
	catch (e){
		parentPort.postMessage({
			filename: f.file,
			error: `Unkown error \"${e}\" while parsing`
		});
		continue;
	}
}
