const charts = {}

const iconHalfSize = 12;
const paddingChar = 10;
const yPadding = 0; // Yaxis pos seems to be affected by the presence of title or no. Should be 0 if title is display, 8 if not
const offset = -2*iconHalfSize - 2*paddingChar;
class LabelPos{
	static Top = new LabelPos(		-iconHalfSize, 		offset);
	static Bottom = new LabelPos(	-iconHalfSize, 		paddingChar);
	static Left = new LabelPos(		offset + yPadding, 	-iconHalfSize);
	static Right = new LabelPos(	-offset + yPadding, -iconHalfSize);

	constructor(offsetX, offsetY) {
		this.offsetX = offsetX;
		this.offsetY = offsetY;
	}
}

function plugin_characters_on_tick(labelPosList){
	return (chart => {
		const ctx = chart.ctx;
		const xAxis = chart.scales["x"];
		const yAxis = chart.scales["y"];
		labelPosList.forEach(pos => {

			const verticalAxis = (pos === LabelPos.Left || pos === LabelPos.Right);
			const curAxis = verticalAxis ? yAxis : xAxis;

			curAxis.ticks.forEach((e, i) => {
				const t = curAxis.getPixelForTick(i);

				const img = char_img[e.label]
				if (img != undefined){
					if (verticalAxis){
						ctx.drawImage(img, (pos === LabelPos.Left ? yAxis.left : yAxis.right) + pos.offsetX, t + pos.offsetY);
					}
					else{
						ctx.drawImage(img, t + pos.offsetX, (pos === LabelPos.Bottom ? yAxis.bottom : yAxis.top) + pos.offsetY);
					}
				}
			});
		});
	});
}
const plugin_tooltip_on_tick = (chart, args) => {
	const tooltip = chart.tooltip;

	const xAxis = chart.scales["x"];
	const yAxis = chart.scales["y"];
	const x = args.event.x, y = args.event.y;
	
	if (y < yAxis.bottom || y > chart.canvas.height - 2){
		return
	}

	const id = xAxis.getValueForPixel(x);
	
	if (id < 0 || id >= xAxis.ticks.length){
		return
	}

	const chartArea = chart.chartArea;
	tooltip.setActiveElements([{
		datasetIndex: 0,
		index: id
	}],
	{
		x: (chartArea.left + chartArea.right) / 2,
		y: (chartArea.top + chartArea.bottom) / 2
	});
}

charts["chart-usage-with"] = new Chart(document.querySelector("canvas#chart-usage-with"), {
	type: "bar",
	plugins: [{
		beforeDraw: plugin_characters_on_tick([LabelPos.Bottom]),
		afterEvent: plugin_tooltip_on_tick
	}],
	data: {
		labels: [],
		datasets: [{
			label: "# of games as",
			data: [],
			backgroundColor: primary_color
		}]
	},
	options: {
		plugins: {
			legend: {
				onClick: null
			}
		},
		layout: {
			padding: {
				bottom: 30
			}
		},
		parsing: {
			xAxisKey: "character",
			yAxisKey: "games"
		},
		scales: {
			y: {
				beginAtZero: true
			},
			x: {
				ticks: {
					display: false
				}
			}
		},
		maintainAspectRatio: false
	}
});
charts["chart-usage-against"] = new Chart(document.querySelector("canvas#chart-usage-against"), {
	type: "bar",
	plugins: [{
		beforeDraw: plugin_characters_on_tick([LabelPos.Bottom]),
		afterEvent: plugin_tooltip_on_tick
	}],
	data: {
		labels: [],
		datasets: [{
			label: "# of games against",
			data: [],
			backgroundColor: primary_color
		}]
	},
	options: {
		plugins: {
			legend: {
				onClick: null
			}
		},
		layout: {
			padding: {
				bottom: 30
			}
		},
		parsing: {
			xAxisKey: "character",
			yAxisKey: "games"
		},
		scales: {
			y: {
				beginAtZero: true
			},
			x: {
				ticks: {
					display: false
				}
			}
		},
		maintainAspectRatio: false
	}
});
charts["chart-winrate-with"] = new Chart(document.querySelector("canvas#chart-winrate-with"), {
	type: "bar",
	plugins: [{
		beforeDraw: plugin_characters_on_tick([LabelPos.Bottom]),
		afterEvent: plugin_tooltip_on_tick
	}],
	data: {
		labels: [],
		datasets: [{
			label: "% of wins as",
			data: [],
			backgroundColor: primary_color
		}]
	},
	options: {
		plugins: {
			legend: {
				onClick: null
			},
			tooltip: {
				callbacks: {
					label: context => [ `${context.dataset.label}: ${(context.raw.winrate).toFixed(2)}%`,
										`W-L: ${(context.raw.wins)}-${(context.raw.losses)}`
										]
							}
			}
		},
		layout: {
			padding: {
				bottom: 30
			}
		},
		parsing: {
			xAxisKey: "character",
			yAxisKey: "winrate"
		},
		scales: {
			y: {
				beginAtZero: true,
				min: 0,
				max: 100,
				ticks: {
					callback: v => `${v}%`
				}
			},
			x: {
				ticks: {
					display: false
				}
			}
		},
		maintainAspectRatio: false
	}
});
charts["chart-winrate-against"] = new Chart(document.querySelector("canvas#chart-winrate-against"), {
	type: "bar",
	plugins: [{
		beforeDraw: plugin_characters_on_tick([LabelPos.Bottom]),
		afterEvent: plugin_tooltip_on_tick
	}],
	data: {
		labels: [],
		datasets: [{
			label: "% of wins against",
			data: [],
			backgroundColor: primary_color
		}]
	},
	options: {
		plugins: {
			legend: {
				onClick: null
			},
			tooltip: {
				callbacks: {
					label: context => [ `${context.dataset.label}: ${(context.raw.winrate).toFixed(2)}%`,
										`W-L: ${(context.raw.wins)}-${(context.raw.losses)}`
										]
				}
			}
		},
		layout: {
			padding: {
				bottom: 30
			}
		},
		parsing: {
			xAxisKey: "character",
			yAxisKey: "winrate"
		},
		scales: {
			y: {
				beginAtZero: true,
				min: 0,
				max: 100,
				ticks: {
					callback: v => `${v}%`
				}
			},
			x: {
				ticks: {
					display: false
				}
			}
		},
		maintainAspectRatio: false
	}
});
charts["chart-usage-stage"] = new Chart(document.querySelector("canvas#chart-usage-stage"), {
	type: "bar",
	plugins: [{
		afterEvent: plugin_tooltip_on_tick
	}],
	data: {
		labels: [],
		datasets: [{
			label: "# of games",
			data: [],
			backgroundColor: primary_color
		}]
	},
	options: {
		plugins: {
			legend: {
				onClick: null
			}
		},
		layout: {
			padding: {
				bottom: 30
			}
		},
		parsing: {
			xAxisKey: "stage",
			yAxisKey: "games"
		},
		scales: {
			y: {
				beginAtZero: true
			},
		},
		maintainAspectRatio: false
	}
});
charts["chart-winrate-stage"] = new Chart(document.querySelector("canvas#chart-winrate-stage"), {
	type: "bar",
	plugins: [{
		afterEvent: plugin_tooltip_on_tick
	}],
	data: {
		labels: [],
		datasets: [{
			label: "% of wins",
			data: [],
			backgroundColor: primary_color
		}]
	},
	options: {
		plugins: {
			legend: {
				onClick: null
			},
			tooltip: {
				callbacks: {
					label: context => `${context.dataset.label}: ${(context.parsed.y).toFixed(2)}%` 
				}
			}
		},
		layout: {
			padding: {
				bottom: 30
			}
		},
		parsing: {
			xAxisKey: "stage",
			yAxisKey: "winrate"
		},
		scales: {
			y: {
				beginAtZero: true,
				min: 0,
				max: 100,
				ticks: {
					callback: v => `${v}%`
				}
			},
		},
		maintainAspectRatio: false
	}
});
charts["chart-winrate-usage"] = new Chart(document.querySelector("canvas#chart-winrate-usage"), {
	type: "scatter",
	data: {
		datasets: [
			{
				type: "scatter",
				label: "Ratio Winrate Against/Opponent Usage",
				data: [],
				backgroundColor: primary_color,
				clip: 20,
				hitRadius: 10,
				parsing: {
					xAxisKey: "games",
					yAxisKey: "winrate"
				},
			},
			{
				type: "line",
				data: [],
				backgroundColor: "gold",
				borderColor: "gold",
				hitRadius: 3,
				pointRadius: 0,
				tooltip: {
					callbacks: {
						label: context => `Global Winrate: ${context.parsed.y.toFixed(2)}%`
					}
				}
			}
		]
	},
	options: {
		plugins: {
			legend: {
				onClick: null
			},
			tooltip: {
				callbacks: {
					label: context => [
						context.raw.character, 
						`Winrate Against: ${context.parsed.y.toFixed(2)}%`,
						`Opponent Usage: ${context.parsed.x.toFixed(2)}%`
					]
				}
			}
		},
		scales: {
			x: {
				type: "linear",
				position: "bottom",
				beginAtZero: true,
				ticks: {
					callback: v => `${v}%`
				}
			},
			y: {
				beginAtZero: true,
				min: 0,
				max: 100,
				ticks: {
					callback: v => `${v}%`
				}
			}
		}
	}
})

ipc.on("stats-usage-with", (e, args) => {
	const data = args.sort((a, b) => b.games - a.games);
	const chart = charts["chart-usage-with"];
	
	chart.data.labels = data.map(x => x.character);
	chart.data.datasets[0].data = data;
	chart.update();
})

ipc.on("stats-usage-against", (e, args) => {
	const data = args.sort((a, b) => b.games - a.games);
	const chart = charts["chart-usage-against"];

	chart.data.labels = data.map(x => x.character);
	chart.data.datasets[0].data = data;
	chart.update();
})

ipc.on("stats-winrate-with", (e, args) => {
	const data = args
		.map(x => ({character: x.character,
					winrate: x.winrate * 100,
					wins: x.wins,
					losses: x.losses}))
		.sort((a, b) => b.winrate - a.winrate);
	const chart = charts["chart-winrate-with"];

	chart.data.labels = data.map(x => x.character);
	chart.data.datasets[0].data = data;
	chart.update();
})

ipc.on("stats-winrate-against", (e, args) => {
	const data = args
		.map(x => ({character: x.character,
					winrate: x.winrate * 100,
					wins: x.wins,
					losses: x.losses}))
		.sort((a, b) => b.winrate - a.winrate);
	const chart = charts["chart-winrate-against"];

	chart.data.labels = data.map(x => x.character);
	chart.data.datasets[0].data = data;
	chart.update();
})

ipc.on("stats-usage-stage", (e, args) => {
	const data = args
		.sort((a, b) => b.games - a.games);
	const chart = charts["chart-usage-stage"];

	chart.data.labels = data.map(x => x.stage);
	chart.data.datasets[0].data = data;
	chart.update();
})

ipc.on("stats-winrate-stage", (e, args) => {
	const data = args
		.map(x => ({stage: x.stage, winrate: x.winrate * 100}))
		.sort((a, b) => b.winrate - a.winrate);
	const chart = charts["chart-winrate-stage"];

	chart.data.labels = data.map(x => x.stage);
	chart.data.datasets[0].data = data;
	chart.update();
})

const lerp = (x, y, a) => x * (1 - a) + y * a;

ipc.on("stats-winrate-usage", (e, args) => {
	const data = args._scatter
		.map(x => ({
			character: x.character,
			games: (args._global.wins + args._global.losses)
				? 100 * x.games / (args._global.wins + args._global.losses) 
				: 0,
			winrate: 100 * x.winrate,
			color: char_color[x.character],
			img: (() => {
				const img = new Image();
				img.src = `./assets/img/characters/${x.character}.png`;
				return img
			})()
		}))
	const chart = charts["chart-winrate-usage"];

	chart.data.labels = data.map(x => x.character);
	chart.data.datasets[0].data = data;
	chart.data.datasets[0].pointBackgroundColor = data.map(x => x.color);
	chart.data.datasets[0].pointStyle = data.map(x => x.img);

	chart.update();

	const line = {
		x1: 0,
		x2: chart.scales.x.end
	}

	chart.data.datasets[1].label = `Global Winrate: ${(100 * args._global.winrate).toFixed(2)}%`
	const nbPts = 20;
	chart.data.datasets[1].data =
		[...Array(nbPts).keys()].map(i => (
			{
				x: lerp(line.x1, line.x2, i / (nbPts-1)),
				y: 100 * args._global.winrate
			}));

	chart.update();
})