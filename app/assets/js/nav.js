const nav_links = document.querySelectorAll("nav a.nav-item")

for (const l of nav_links){
	const page_id = l.href.substring(l.href.indexOf("#") + 1);
	
	l.addEventListener("click", function(e){
		e.preventDefault();
		
		for (const s of document.querySelectorAll("main > section")){
			s.classList.add("hidden");
		}

		document.querySelector(`main section#${page_id}`).classList.remove("hidden");
	})
}