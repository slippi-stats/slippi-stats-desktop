const evolution_charts = {}


function format_date(epoch)
{
	return new Date(epoch).toLocaleDateString().split('T')[0].replaceAll('-', '/');
}

const half_width_days = 15;
const half_width_epoch = half_width_days * 60 * 60 * 24 * 1000;

function compute_smoothed_data(cur_data, data)
{
	// moving average
	const data_in_window = data.filter((x) => {
		return Math.abs(x.datetime.getTime() - cur_data.datetime.getTime()) <= half_width_epoch;
		}
	);
	const nb_wins = data_in_window.reduce((acc, b) => acc + b.wins, 0);
	const nb_losses = data_in_window.reduce((acc, b) => acc + b.losses, 0);
	const smoothed_winrate = (nb_wins + nb_losses) > 0
		? 100*nb_wins / (nb_wins + nb_losses)
		: cur_data.winrate;

	return {
		winrate: smoothed_winrate,
		datetime: cur_data.datetime,
		wins: nb_wins,
		losses: nb_losses,
		ties: cur_data.ties
	}
}

evolution_charts["chart-evolution-winrate-over-time"] = new Chart(document.querySelector("canvas#chart-evolution-winrate-over-time"), {
	type: "scatter", // not "line" otherwise labels are showing dates raw data
	data: {
		datasets: [
			{
				type: "scatter",
				label: "Daily Winrate",
				data: [],
				backgroundColor: secondary_color,
				borderColor: secondary_color,
				pointRadius: 1,
				parsing: {
					xAxisKey: "datetime",
					yAxisKey: "winrate"
				},
				tooltip: {
					callbacks: {
						label: context => [
							`${format_date(context.parsed.x)}`,
							`Winrate: ${context.parsed.y.toFixed(2)}%`,
							`W-L: ${context.raw.wins}-${context.raw.losses}`
						]
					}
				}
			},
			{
				type: "line",
				borderCapStyle: "round",
				borderJoinStyle: "round",
				pointRadius: 0,
				label: `Smoothed Winrate (${2*half_width_days} days window)`,
				data: [],
				backgroundColor: primary_color,
				borderColor: primary_color,
				parsing: {
					xAxisKey: "datetime",
					yAxisKey: "winrate"
				},
				tooltip: {
					callbacks: {
						label: context => [
							`${format_date(context.parsed.x)}`,
							`Smoothed Winrate: ${context.parsed.y.toFixed(2)}%`,
							`W-L: ${context.raw.wins}-${context.raw.losses} (${2*half_width_days} days window)`
						]
					}
				}
			}
		]
	},
	options: {
		plugins: {
			legend: {
				onClick: null
			},
		},
		layout: {
			padding: 30
		},
		scales: {
			x: {
				type: "linear",
				position: "bottom",
				ticks: {
					callback: x => `${format_date(x)}`
				}
			},
			y: {
				min: 0,
				max: 100,
				ticks: {
					callback: v => `${v}%`
				}
			}
		}
	}
})

ipc.on("stats-winrate-over-time", (e, args) => {
	const chart = evolution_charts["chart-evolution-winrate-over-time"];
	const data = args
		.map(x => ({
			winrate: 100.0 * x.winrate,
			datetime: x.datetime,
			wins: x.wins,
			losses: x.losses,
			ties: x.ties
		}))

	chart.data.labels = data.map(x => x.datetime);
	chart.data.datasets[0].data = data;

	const data_smoothed = data
		.map(x => compute_smoothed_data(x, data));

	chart.data.datasets[1].data = data_smoothed;

	chart.update();
})