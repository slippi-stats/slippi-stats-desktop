const matchups_charts = {}

const char_list = Object.keys(char_color).filter(x => x !== "Others");

// gradient red -> yellow -> green
function color_from_winrate(winrate)
{
	if (winrate < 0){
		return 'rgba(180, 180, 180, 1.0)'; // light gray
	}
	const green = 	[0.282, 0.560, 0.192];
	const yellow = 	[0.854, 0.780, 0.403];
	const red = 	[0.870, 0.258, 0.356];

	var res = 		[0,0,0];
	if (winrate > 0.5){
		const t = 2*(winrate - 0.5)
		res = [ lerp(yellow[0], green[0], t),
				lerp(yellow[1], green[1], t),
				lerp(yellow[2], green[2], t) ];
	}
	else{
		const t = 2*winrate;
		res = [ lerp(red[0], yellow[0], t),
				lerp(red[1], yellow[1], t),
				lerp(red[2], yellow[2], t) ];
	}

	const resStr = `rgba(${255*res[0]}, ${255*res[1]}, ${255*res[2]}, 1.0)`;
	return resStr;
}


matchups_charts["chart-matchups"] = new Chart(document.querySelector("canvas#chart-matchups"), {
	type: "bubble",
	title: "Matchups",
	plugins: [{
		beforeDraw: plugin_characters_on_tick([LabelPos.Top, LabelPos.Left])
		//afterEvent: plugin_tooltip_on_tick // TODO maybe ?
	}],	
	data: {
		labels: [],
		datasets: [
			{
				type: "bubble",
				data: [],
				pointBackgroundColor: [],
				label: "Matchup's Winrates",

				pointStyle: "rect",
				pointRadius: 20, // TODO adapt point size to chart size

				tooltip: {
					callbacks: {
						label: context => [
							`Playing ${context.raw.char_player} against ${context.raw.char_opponent}`,
							context.raw.winrate >= 0 ? `Winrate: ${(100.0*context.raw.winrate).toFixed(2)}%` : '',
							`W-L: ${context.raw.wins}-${context.raw.losses}`
						]
					}
				},
			},
		]
	},

	options: {
		plugins: {
			legend: {
				onClick: null,
				display: false
			},
		},
		layout: {
			padding: 80
		},	
		parsing: {
			xAxisKey: "char_opponent",
			yAxisKey: "char_player"
		},
		scales: {
			x: {
				type: 'category',
				labels: char_list,
				position: "top",
				ticks: {
					display: false
				},
				title: {
					display: true,
					text: "Opponent",
					padding: -50-24,
					font: {
						size: 18
					}					
				}

			},
			y: {
				type: 'category',
				labels: char_list,
				position: "left",
				ticks: {
					display: false
				},
				title: {
					display: true,
					text: "Player",
					padding: -50-24,
					font: {
						size: 18
					}
				}
			}
		},
		aspectRatio: 1,
		maintainAspectRatio: true
	}
})


ipc.on("stats-matchups", (e, args) => {


	const chart = matchups_charts["chart-matchups"];

	var data = args.map(x => ({
		char_player : x.char_player,
		char_opponent : x.char_opponent,
		winrate : x.winrate,
		wins : x.wins,
		losses : x.losses
	}));

	// filling missing data 
	for (char_player of  char_list){
		for (char_opponent of  char_list){
			const found = data.find(x => x.char_player === char_player && x.char_opponent === char_opponent)
			if (found == undefined){
				data.push({
					char_player : char_player,
					char_opponent : char_opponent,
					winrate : -1,
					wins : 0,
					losses : 0
				});
			}
		}
	}

	pointBackgroundColor = data.map(x =>
		color_from_winrate(x.winrate));

	chart.data.datasets[0].data = data;
	chart.data.datasets[0].pointBackgroundColor = pointBackgroundColor;

	chart.update();
})
