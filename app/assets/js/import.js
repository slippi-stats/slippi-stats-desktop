const form_import = document.querySelector("section#import form#import");
const form_settings = document.querySelector("section#import form#settings")
let timer_import;

ipc.send("settings", { method: "GET" })

ipc.on("settings", (e, args) => {
	if (args.method == "GET"){
		form_settings.querySelector("input#player_code").value = args.data.player_code
		form_settings.querySelector("input#replays_dir").value = args.data.replays_dir
		document.querySelector("main .versus .player").textContent = args.data.player_code
	}
	else if (args.method == "POST"){
		if (args.status == "ok"){
			form_settings.querySelector("span#status").textContent = "Updated successfully!"
		}
	}
})

form_settings.querySelector("input#replays_dir_path").addEventListener("change", function(e){
	const dir = path.dirname(this.files.item(0).path);
	form_settings.querySelector("input#replays_dir").value = dir;
});

form_settings.addEventListener("submit", e => {
	e.preventDefault();

	form_settings.querySelector("span#status").textContent = "Updating..."

	const settings = {
		player_code: form_settings.querySelector("input#player_code").value,
		replays_dir: form_settings.querySelector("input#replays_dir").value
	}

	document.querySelector("main .versus .player").textContent = settings.player_code;

	ipc.send("settings", {
		method: "POST",
		settings: settings
	})
});

form_import.addEventListener("submit", function(e){
	e.preventDefault();

	ipc.send("parse");
	timer_import = setInterval(() => {
		ipc.send("progress");
	}, 1000)
})

ipc.on("progress", (e, args) => {
	const table = document.querySelector("section#import .stats table");

	table.querySelector("#import-stats-total").textContent = args.total;
	table.querySelector("#import-stats-imported").textContent = args.imported;
	table.querySelector("#import-stats-errors").textContent = args.errors;
	table.querySelector("#import-stats-ignored").textContent = args.ignored;
	table.querySelector("#import-stats-progress").textContent = `${((args.errors + args.ignored + args.imported) / args.total * 100).toFixed(2)}%`;

	const logs = document.querySelector("section#import .logs .output");

	while (logs.lastElementChild){
		logs.removeChild(logs.lastElementChild);
	}

	for (l of args.logs){
		const type = l[0];
		const message = l[1];

		const element = document.createElement("span");
		element.appendChild(document.createTextNode(message));
		if (type === "SUCCESS"){
			element.classList.add("text-success");
		}
		else if (type === "INFO"){
			element.classList.add("text-warning");
		}
		else if (type === "ERROR"){
			element.classList.add("text-error");
		}

		if (logs.firstChild){
			logs.insertBefore(element, logs.firstChild);
		}
		else{
			logs.appendChild(element);
		}
	}
	
	if (!args.locked){
		clearInterval(timer_import);
	}
})