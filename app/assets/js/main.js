// Libraries
const path = require("path");
const fs = require("fs");
const ipc = require("electron").ipcRenderer;
const Chart = require("chart.js");

const char_color = {
	"Dr. Mario": "#E5E5E5",
	"Mario": "#CB4030",
	"Luigi": "#487C44",
	"Bowser": "#D35A29",
	"Peach": "#D46776",
	"Yoshi": "#92F92F",
	"Donkey Kong": "#572F1D",
	"Captain Falcon": "#5E61BE",
	"Ganondorf": "#4E4333",
	"Fox": "#E38637",
	"Falco": "#424CC8",
	"Ness": "#E8D6A6",
	"Ice Climbers": "#ADD6E8",
	"Kirby": "#DD599E",
	"Samus": "#74201E",
	"Zelda": "#BA96F1",
	"Sheik": "#455D8A",
	"Link": "#68AB51",
	"Young Link": "#C4E238",
	"Pichu": "#FCF134",
	"Pikachu": "#E8DD15",
	"Jigglypuff": "#F3D3E8",
	"Mewtwo": "#737290",
	"Mr. Game & Watch": "#000000",
	"Marth": "#574A75",
	"Roy": "#390C12",
	"Others": "#808080"
}

function gen_img(character)
{
	const img = new Image();
	img.src = `./assets/img/characters/${character}.png`;
	return img;
}

const char_img = {}
Object.keys(char_color).map(char_name => char_img[char_name] = gen_img(char_name));

const primary_color = "#0d6efd"; // blue
const secondary_color = "#6c757d"; // gray-600

const filters = {
	opponent_code: "",
	with: [],
	against: [],
	date_after: "",
	date_before: "",
	stage: [],
	unique: false
}

const button_collapse_filters = document.getElementById("collapse-button-filters");
const button_expand_filters = document.getElementById("expand-button-filters");
const form = document.querySelector("aside#filters form");

function toggle_filters(){
	console.log("toto");
	document.querySelector("body").classList.toggle("filters-expanded");
}

button_collapse_filters.addEventListener("click", toggle_filters);
button_expand_filters.addEventListener("click", toggle_filters);

form.querySelector("input#player_code").addEventListener("change", function(){filters.opponent_code = this.value});
form.querySelector("select#with").addEventListener("change", function(){filters.with = Array.from(this.querySelectorAll("option:checked")).map(x => x.value)})
form.querySelector("select#against").addEventListener("change", function(){filters.against = Array.from(this.querySelectorAll("option:checked")).map(x => x.value)})
form.querySelector("input#date_after").addEventListener("change", function(){filters.date_after = this.value});
form.querySelector("input#date_before").addEventListener("change", function(){filters.date_before = this.value});
form.querySelector("select#stage").addEventListener("change", function(){filters.stage = Array.from(this.querySelectorAll("option:checked")).map(x => x.value)});
form.querySelector("input#unique").addEventListener("change", function(){filters.unique = this.checked});

form.addEventListener("submit", (e) => {
	e.preventDefault();
	toggle_filters();
	get_stats();
});

ipc.on("stats-global", (e, args) => {
	const global_stats = document.querySelector(".global-stats");

	global_stats.querySelector(".games .value").textContent = args.games;
	global_stats.querySelector(".wins .value").textContent = args.wins;
	global_stats.querySelector(".losses .value").textContent = args.losses;
	global_stats.querySelector(".ties .value").textContent = args.ties;
	global_stats.querySelector(".winrate .value").textContent = `${(100 * args.winrate).toFixed(2)}%`;
});

function get_stats(){
	ipc.send("stats", filters);
}

get_stats();