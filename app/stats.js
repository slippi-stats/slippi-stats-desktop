const { PrismaClient } = require("@prisma/client");

const prisma = new PrismaClient();

const build_where_clause = (filters, others) => Object.assign({}, filters, others)
const build_date_where_clause = (after, before) => {
	if (!after && !before){
		return undefined
	}

	const res = {};

	if (after){
		res.gte = new Date(after)
	}

	if (before){
		res.lte = new Date(before)
	}

	return res;
}

function compute_winrate(wins, losses){
	return (wins + losses) > 0
			? (wins / (losses + wins))
			: 0;
}

module.exports.stats = (e, args) => {
	(async () => {
		const filters = {
			opponent: args.opponent_code ? args.opponent_code : undefined,
			char_player: args.with.length > 0 ? {
				in: args.with
			} : undefined,
			char_opponent: args.against.length > 0 ? {
				in: args.against
			} : undefined,
			datetime: build_date_where_clause(args.date_after, args.date_before),
			stage: args.stage.length > 0 ? {
				in: args.stage
			} : undefined
		}

		const _global = {};

		if (args.unique !== true){
			_global.games = await prisma.game.count({
				where: build_where_clause(filters)
			});
			_global.wins = await prisma.game.count({
				where: build_where_clause(filters, {
					win: true
				})
			});
			_global.losses = await prisma.game.count({
				where: build_where_clause(filters, {
					win: false
				})
			});
			_global.ties = await prisma.game.count({
				where: build_where_clause(filters, {
					win: null
				})
			});
		}
		else{
			const games = await prisma.game.groupBy({
				by: ["opponent"],
				where: build_where_clause(filters)
			});
			_global.games = games.length;
			const groupBy = {
				wins: await prisma.game.groupBy({
					by: ["opponent"],
					where: build_where_clause(filters, {
						win: true
					}),
					_count: {
						_all: true
					}
				}),
				losses: await prisma.game.groupBy({
					by: ["opponent"],
					where: build_where_clause(filters, {
						win: false
					}),
					_count: {
						_all: true
					}
				}),
				ties: await prisma.game.groupBy({
					by: ["opponent"],
					where: build_where_clause(filters, {
						win: null
					}),
					_count: {
						_all: true
					}
				})
			};

			_global.wins = 0, _global.losses = 0, _global.ties = 0;

			for (x of games){
				const opponent_code = x.opponent;
				const entries = {
					wins: groupBy.wins.find(x => x.opponent === opponent_code),
					losses: groupBy.losses.find(x => x.opponent === opponent_code)
				}
				const wins = entries.wins ? entries.wins._count._all : 0;
				const losses = entries.losses ? entries.losses._count._all : 0;

				if (wins > losses){
					_global.wins++;
				}
				else if (wins < losses){
					_global.losses++;
				}
				else{
					_global.ties++;
				}
			}
		}

		_global.winrate = compute_winrate(_global.wins, _global.losses);

		let _with;

		if (args.unique !== true){
			_with = (await prisma.game.groupBy({
				by: ["char_player"],
				_count: {
					_all: true,
					win: true,
				},
				where: build_where_clause(filters)
			}))
				.map(x => ({
					character: x.char_player,
					games: x._count._all,
					ties: x._count._all - x._count.win
				}));
	
			const _with_wins = (await prisma.game.groupBy({
				by: ["char_player"],
				_count: {
					_all: true
				},
				where: build_where_clause(filters, {
					win: true
				})
			}))
				.map(x => ({
					character: x.char_player,
					wins: x._count._all
				}));

			for (g of _with){
				const w = _with_wins.find(x => x.character == g.character)
				g.wins = w ? w.wins : 0
				g.losses = g.games - g.wins - g.ties
			}
		}
		else{
			const games_with = (await prisma.game.groupBy({
				by: ["char_player", "opponent"],
				_count: {
					_all: true,
				},
				where: build_where_clause(filters)
			}))
				.map(x => ({
					character: x.char_player,
					opponent: x.opponent,
					games: x._count._all
				}));

			_with = games_with
				.reduce((acc, e) => {
					const character = acc.find(x => x.character == e.character);

					if (character){
						character.games++;
					}
					else {
						acc.push({
							character: e.character,
							games: 1,
							wins: 0,
							losses: 0,
							ties: 0
						});
					}
					return acc;
				}, []);

			const groupBy = {
				wins: (await prisma.game.groupBy({
					by: ["char_player", "opponent"],
					_count: {
						_all: true
					},
					where: build_where_clause(filters, {
						win: true
					})
				}))
					.map(x => ({
						character: x.char_player,
						opponent: x.opponent,
						wins: x._count._all
					})),
				losses: (await prisma.game.groupBy({
					by: ["char_player", "opponent"],
					_count: {
						_all: true
					},
					where: build_where_clause(filters, {
						win: false
					})
				}))
					.map(x => ({
						character: x.char_player,
						opponent: x.opponent,
						losses: x._count._all
					}))
			}
			
			for (c of games_with){
				const opponent_code = c.opponent;
				const character = c.character;

				const entries = {
					wins: groupBy.wins.find(x => x.opponent === opponent_code && x.character === character),
					losses: groupBy.losses.find(x => x.opponent === opponent_code && x.character === character)
				}

				const wins = entries.wins ? entries.wins.wins : 0;
				const losses = entries.losses ? entries.losses.losses : 0;

				if (wins > losses){
					_with.find(x => x.character === character).wins++;
				}
				else if (wins < losses){
					_with.find(x => x.character === character).losses++;
				}
				else{
					_with.find(x => x.character === character).ties++;
				}
			}
		}

		for (g of _with){
			g.winrate = compute_winrate(g.wins, g.losses);
		}

		let _against;

		if (args.unique !== true){
			_against = (await prisma.game.groupBy({
				by: ["char_opponent"],
				_count: {
					_all: true,
					win: true
				},
				where: build_where_clause(filters)
			}))
				.map(x => ({
					character: x.char_opponent,
					games: x._count._all,
					ties: x._count._all - x._count.win
				}));
			
			const _against_wins = (await prisma.game.groupBy({
				by: ["char_opponent"],
				_count: {
					_all: true
				},
				where: build_where_clause(filters, {
					win: true
				})
			}))
				.map(x => ({
					character: x.char_opponent,
					wins: x._count._all
				}));

			for (g of _against){
				const w = _against_wins.find(x => x.character == g.character)
				g.wins = w ? w.wins : 0
				g.losses = g.games - g.wins - g.ties
			}
		}
		else{
			const games_against = (await prisma.game.groupBy({
				by: ["char_opponent", "opponent"],
				_count: {
					_all: true
				},
				where: build_where_clause(filters)
			}))
				.map(x => ({
					character: x.char_opponent,
					opponent: x.opponent,
					games: x._count._all
				}));

			_against = games_against
				.reduce((acc, e) => {
					const character = acc.find(x => x.character == e.character);

					if (character){
						character.games++;
					}
					else {
						acc.push({
							character: e.character,
							games: 1,
							wins: 0,
							losses: 0,
							ties: 0
						});
					}

					return acc;
				}, []);

			const groupBy = {
				wins: (await prisma.game.groupBy({
					by: ["char_opponent", "opponent"],
					_count: {
						_all: true
					},
					where: build_where_clause(filters, {
						win: true
					})
				}))
					.map(x => ({
						character: x.char_opponent,
						opponent: x.opponent,
						wins: x._count._all
					})),
				losses: (await prisma.game.groupBy({
					by: ["char_opponent", "opponent"],
					_count: {
						_all: true
					},
					where: build_where_clause(filters, {
						win: false
					})
				}))
					.map(x => ({
						character: x.char_opponent,
						opponent: x.opponent,
						losses: x._count._all
					}))
			}

			for (c of games_against){
				const opponent_code = c.opponent;
				const character = c.character;

				const entries = {
					wins: groupBy.wins.find(x => x.opponent === opponent_code && x.character === character),
					losses: groupBy.losses.find(x => x.opponent === opponent_code && x.character === character)
				}

				const wins = entries.wins ? entries.wins.wins : 0;
				const losses = entries.losses ? entries.losses.losses : 0;

				if (wins > losses){
					_against.find(x => x.character === character).wins++;
				}
				else if (wins < losses){
					_against.find(x => x.character === character).losses++;
				}
				else{
					_against.find(x => x.character === character).ties++;
				}
			}
		}
		
		for (g of _against){
			g.winrate = compute_winrate(g.wins, g.losses);
		}

		let _stages;

		if (args.unique !== true){
			_stages = (await prisma.game.groupBy({
				by: ["stage"],
				_count: {
					_all: true,
					win: true
				},
				where: build_where_clause(filters)
			}))
				.map(x => ({
					stage: x.stage,
					games: x._count._all,
					ties: x._count._all - x._count.win
				}));
	
			const _stages_wins = (await prisma.game.groupBy({
				by: ["stage"],
				_count: {
					_all: true
				},
				where: build_where_clause(filters, {
					win: true
				})
			}))
				.map(x => ({
					stage: x.stage,
					wins: x._count._all
				}));
			
			for (g of _stages){
				const w = _stages_wins.find(x => x.stage == g.stage)
				g.wins = w ? w.wins : 0
				g.losses = g.games - g.wins - g.ties
			}
		}
		else{
			const games_stages = (await prisma.game.groupBy({
				by: ["stage", "opponent"],
				_count: {
					_all: true
				},
				where: build_where_clause(filters)
			}))
				.map(x => ({
					stage: x.stage,
					opponent: x.opponent,
					games: x._count._all
				}));

			_stages = games_stages
				.reduce((acc, e) => {
					const stage = acc.find(x => x.stage == e.stage);

					if (stage){
						stage.games++;
					}
					else {
						acc.push({
							stage: e.stage,
							games: 1,
							wins: 0,
							losses: 0,
							ties: 0
						});
					}

					return acc;
				}, []);

			const groupBy = {
				wins: (await prisma.game.groupBy({
					by: ["stage", "opponent"],
					_count: {
						_all: true
					},
					where: build_where_clause(filters, {
						win: true
					})
				}))
					.map(x => ({
						stage: x.stage,
						opponent: x.opponent,
						wins: x._count._all
					})),
				losses: (await prisma.game.groupBy({
					by: ["stage", "opponent"],
					_count: {
						_all: true
					},
					where: build_where_clause(filters, {
						win: false
					})
				}))
					.map(x => ({
						stage: x.stage,
						opponent: x.opponent,
						losses: x._count._all
					}))
			}

			for (c of games_stages){
				const opponent_code = c.opponent;
				const stage = c.stage;

				const entries = {
					wins: groupBy.wins.find(x => x.opponent === opponent_code && x.stage === stage),
					losses: groupBy.losses.find(x => x.opponent === opponent_code && x.stage === stage)
				}

				const wins = entries.wins ? entries.wins.wins : 0;
				const losses = entries.losses ? entries.losses.losses : 0;

				if (wins > losses){
					_stages.find(x => x.stage === stage).wins++;
				}
				else if (wins < losses){
					_stages.find(x => x.stage === stage).losses++;
				}
				else{
					_stages.find(x => x.stage === stage).ties++;
				}
			}
		}

		for (g of _stages){
			g.winrate = compute_winrate(g.wins, g.losses);
		}

		let _per_day;

		if (args.unique !== true){
			_per_day = (await prisma.game.groupBy({
				by: ["datetime"],
				_count: {
					_all: true,
					win: true,
				},
				where: build_where_clause(filters)
			}))
				.map(x => ({
					datetime: x.datetime,
					games: x._count._all,
					ties: x._count._all - x._count.win
				}))
			
			const _wins_this_day = (await prisma.game.groupBy({
				by: ["datetime"],
				_count: {
					_all: true
				},
				where: build_where_clause(filters, {
					win: true
				})
			}))
				.map(x => ({
					datetime: x.datetime,
					wins: x._count._all
				}))
	
			for (g of _per_day){
				const w = _wins_this_day.find( x => x.datetime.getTime() === g.datetime.getTime() );
				g.wins = w ? w.wins : 0;
				g.losses = g.games - g.wins - g.ties;
				g.winrate = compute_winrate(g.wins, g.losses); 
			}
		}
		else{
			const games_per_day = (await prisma.game.groupBy({
				by: ["datetime", "opponent"],
				_count: {
					_all: true
				},
				where: build_where_clause(filters)
			}))
				.map(x => ({
					datetime: x.datetime,
					opponent: x.opponent,
					games: x._count._all
				}));

			_per_day = games_per_day
				.reduce((acc, e) => {
					const datetime = acc.find(x => x.datetime - e.datetime == 0);

					if (datetime){
						datetime.games++;
					}
					else {
						acc.push({
							datetime: e.datetime,
							games: 1,
							wins: 0,
							losses: 0,
							ties: 0
						});
					}

					return acc;
				}, []);
			
			const groupBy = {
				wins: (await prisma.game.groupBy({
					by: ["datetime", "opponent"],
					_count: {
						_all: true
					},
					where: build_where_clause(filters, {
						win: true
					})
				}))
					.map(x => ({
						datetime: x.datetime,
						opponent: x.opponent,
						wins: x._count._all
					})),
				losses: (await prisma.game.groupBy({
					by: ["datetime", "opponent"],
					_count: {
						_all: true
					},
					where: build_where_clause(filters, {
						win: false
					})
				}))
					.map(x => ({
						datetime: x.datetime,
						opponent: x.opponent,
						losses: x._count._all
					}))
			}

			for (c of games_per_day){
				const opponent_code = c.opponent;
				const datetime = c.datetime;

				const entries = {
					wins: groupBy.wins.find(x => x.opponent === opponent_code && x.datetime - datetime == 0),
					losses: groupBy.losses.find(x => x.opponent === opponent_code && x.datetime - datetime == 0)
				}

				const wins = entries.wins ? entries.wins.wins : 0;
				const losses = entries.losses ? entries.losses.losses : 0;

				if (wins > losses){
					_per_day.find(x => x.datetime - datetime == 0).wins++;
				}
				else if (wins < losses){
					_per_day.find(x => x.datetime - datetime == 0).losses++;
				}
				else{
					_per_day.find(x => x.datetime - datetime == 0).ties++;
				}
			}
		}

		for (g of _per_day){
			g.winrate = compute_winrate(g.wins, g.losses); 
		}

		let _matchups;

		if (args.unique !== true){
			_matchups = (await prisma.game.groupBy({
				by: ["char_player", "char_opponent"],
				_count: {
					_all: true,
					win: true,
				},
				where: build_where_clause(filters)
			}))
				.map(x => ({
					char_player: x.char_player,
					char_opponent: x.char_opponent,
					games: x._count._all,
					ties: x._count._all - x._count.win
				}));
	
			const _matchups_wins = (await prisma.game.groupBy({
				by: ["char_player", "char_opponent"],
				_count: {
					_all: true
				},
				where: build_where_clause(filters, {
					win: true
				})
			}))
				.map(x => ({
					char_player: x.char_player,
					char_opponent: x.char_opponent,
					wins: x._count._all
				}));
			
			for (g of _matchups){
				const w = _matchups_wins.find(x => x.char_player == g.char_player && x.char_opponent == g.char_opponent)
				g.wins = w ? w.wins : 0
				g.losses = g.games - g.wins - g.ties
				g.winrate = compute_winrate(g.wins, g.losses);
			}
		}
		else{
			const games_matchups = (await prisma.game.groupBy({
				by: ["char_player", "char_opponent", "opponent"],
				_count: {
					_all: true
				},
				where: build_where_clause(filters)
			}))
				.map(x => ({
					char_player: x.char_player,
					char_opponent: x.char_opponent,
					opponent: x.opponent,
					games: x._count._all
				}));
			
			_matchups = games_matchups
				.reduce((acc, e) => {
					const matchups = acc.find(x => x.char_player === e.char_player && x.char_opponent === e.char_opponent);

					if (matchups){
						matchups.games++;
					}
					else{
						acc.push({
							char_player: e.char_player,
							char_opponent: e.char_opponent,
							games: 1,
							wins: 0,
							losses: 0,
							ties: 0
						});
					}

					return acc;
				}, []);

			const groupBy = {
				wins: (await prisma.game.groupBy({
					by: ["char_player", "char_opponent", "opponent"],
					_count: {
						_all: true
					},
					where: build_where_clause(filters, {
						win: true
					})
				}))
					.map(x => ({
						char_player: x.char_player,
						char_opponent: x.char_opponent,
						opponent: x.opponent,
						wins: x._count._all
					})),
				losses: (await prisma.game.groupBy({
					by: ["char_player", "char_opponent", "opponent"],
					_count: {
						_all: true
					},
					where: build_where_clause(filters, {
						win: false
					})
				}))
					.map(x => ({
						char_player: x.char_player,
						char_opponent: x.char_opponent,
						opponent: x.opponent,
						losses: x._count._all
					})) 
			}

			for (c of games_matchups){
				const opponent_code = c.opponent;
				const char_player = c.char_player;
				const char_opponent = c.char_opponent;

				const entries = {
					wins: groupBy.wins.find(x => x.opponent === opponent_code && x.char_player === char_player && x.char_opponent === char_opponent),
					losses: groupBy.losses.find(x => x.opponent === opponent_code && x.char_player === char_player && x.char_opponent === char_opponent)
				}

				const wins = entries.wins ? entries.wins.wins : 0;
				const losses = entries.losses ? entries.losses.losses : 0;

				if (wins > losses){
					_matchups.find(x => x.char_player === char_player && x.char_opponent === char_opponent).wins++;
				}
				else if (wins < losses){
					_matchups.find(x => x.char_player === char_player && x.char_opponent === char_opponent).losses++;
				}
				else{
					_matchups.find(x => x.char_player === char_player && x.char_opponent === char_opponent).ties++;
				}
			}

		}

		for (g of _matchups){
			g.winrate = compute_winrate(g.wins, g.losses);
		}

		// HOME
		e.sender.send("stats-global", _global)
		e.sender.send("stats-usage-with", _with)
		e.sender.send("stats-winrate-with", _with)
		e.sender.send("stats-usage-against", _against)
		e.sender.send("stats-winrate-against", _against)
		e.sender.send("stats-usage-stage", _stages)
		e.sender.send("stats-winrate-stage", _stages)
		e.sender.send("stats-winrate-usage", {_global: _global, _scatter: _against})

		// EVOLUTION
		e.sender.send("stats-winrate-over-time", _per_day)

		// MATCHUPS 
		e.sender.send("stats-matchups", _matchups)
	})()
		.catch(e => {
			throw e
		})
		.finally(async () => {
			await prisma.$disconnect()
		})
}