const winston = require("winston");

const logger = winston.createLogger({
	level: "info",
	format: winston.format.json(),
	defaultMeta: { service: "user-service" },
	transports: [
		new winston.transports.File({ filename: "error.log", level: "error" }),
		new winston.transports.File({ filename: "combined.log" })
	]
})

const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();

(async () => {
	const settings = [
		{ key: "player_code", value: "" },
		{ key: "replays_dir", value: "" }
	]

	await Promise.all(
		settings.map(async s => {
			logger.info(`Checking for ${s.key} in Settings`)
			const row = await prisma.settings.findUnique({
				where: {
					key: s.key
				}
			})

			if (!row){
				logger.info(`Initialization of Settings table in db with ${s.key}`)				
				await prisma.settings.create({
					data: s
				})
			}
		})
	)
})()
	.catch(e => { throw e })
	.finally(async () => { await prisma.$disconnect })

module.exports.settings = (e, args) => {
	if (args.method === "GET"){
		(async () => {
			const settings = await prisma.settings.findMany()
			const data = {}
			
			for (s of settings){
				data[s.key] = s.value
			}
		
			const res = {
				method: "GET",
				status: "ok",
				data: data
			}
			
			e.sender.send("settings", res)
		})()
			.catch(e => { throw e })
			.finally(async () => { await prisma.$disconnect })
	}
	else if (args.method === "POST"){
		(async () => {
			const settings = args.settings

			await prisma.settings.update({
				where: { key: "player_code" },
				data: { value: settings.player_code }
			})

			await prisma.settings.update({
				where: { key: "replays_dir" },
				data: { value: settings.replays_dir }
			})

			const res = {
				method: "POST",
				status: "ok",
				data: settings 
			}

			e.sender.send("settings", res)
		})()
			.catch(e => { throw e })
			.finally(async () => { await prisma.$disconnect })
	}
}